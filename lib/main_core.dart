import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_i18n/loaders/decoders/json_decode_strategy.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:psono/app_config.dart';
import 'package:psono/model/app_state.dart';
import 'package:psono/protected_screen.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/account/index.dart';
import 'package:psono/screens/autofill_onboarding/index.dart';
import 'package:psono/screens/datastore/index.dart';
import 'package:psono/screens/home/index.dart';
import 'package:psono/screens/lost_password/index.dart';
import 'package:psono/screens/pin/index.dart';
import 'package:psono/screens/pin_configuration/index.dart';
import 'package:psono/screens/privacy_policy/index.dart';
import 'package:psono/screens/register/index.dart';
import 'package:psono/screens/settings/index.dart';
import 'package:psono/screens/signin/index.dart';
import 'package:psono/theme.dart';
import 'package:redux/redux.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context)!;
    final Store store = reduxStore;

    return StoreProvider<AppState>(
      store: store as Store<AppState>,
      child: MaterialApp(
          title: config.appName,
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              canvasColor: primarySwatch.shade500,
              brightness: Brightness.light,
              fontFamily: 'Open Sans',
              colorScheme: ColorScheme.fromSwatch(primarySwatch: primarySwatch)
                  .copyWith(secondary: primarySwatch.shade500),
              scaffoldBackgroundColor: const Color(0xFF151f2b)),
          initialRoute: '/',
          routes: {
            '/': (context) => HomeScreen(),
            '/account/': (context) => ProtectedScreen(child: AccountScreen()),
            '/autofill/': (context) =>
                ProtectedScreen(child: DatastoreScreen()),
            '/datastore/': (context) =>
                ProtectedScreen(child: DatastoreScreen()),
            '/autofill_onboarding/': (context) => AutofillOnboardingScreen(),
            '/lost_password/': (context) => LostPasswordScreen(),
            '/passphrase/': (context) => PinScreen(),
            '/privacy_policy/': (context) => PrivacyPolicyScreen(),
            '/settings/': (context) => ProtectedScreen(child: SettingsScreen()),
            '/signin/': (context) => SigninScreen(),
            '/pin_configuration_screen/': (context) => PinConfigurationScreen(),
            '/register/': (context) => RegisterScreen(),
          },
          supportedLocales: const [
            // ATTENTION: leave en this as first language as this is the default
            Locale('en'), // English
            Locale('bn'), // Bengali
            Locale('ca'), // Catalan
            Locale('cs'), // Czech
            Locale('de'), // German
            Locale('es'), // Spanish
            Locale('fi'), // Finnish
            Locale('fr'), // French
            Locale('hu'), // Hungarian
            Locale('hr'), // Croatian
            Locale('it'), // Italian
            // Locale('nb'), // Norwegian
            // Locale('nn'), // Norwegian
            Locale('sv'), // Swedish
            Locale('uk'), // Ukrainian
          ],
          localizationsDelegates: [
            FlutterI18nDelegate(
              translationLoader: FileTranslationLoader(
                fallbackFile: 'en',
                basePath: "assets/locales",
                decodeStrategies: [JsonDecodeStrategy()],
              ),
              missingTranslationHandler: (key, locale) {
                print(
                    "--- Missing Key: $key, languageCode: ${locale!.languageCode}");
              },
            ),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          builder: FlutterI18n.rootAppBuilder() //If you want to support RTL.
          ),
    );
  }
}
