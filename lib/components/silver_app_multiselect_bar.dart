import 'package:flutter/material.dart';

class SliverAppMultiselectBar extends StatefulWidget {
  static String tag = 'datastore-screen';

  final VoidCallback? onDelete;
  final VoidCallback? onEdit;
  final int? selectedElements;

  SliverAppMultiselectBar({
    this.onDelete,
    this.onEdit,
    this.selectedElements,
  });

  @override
  _SliverAppMultiselectBarState createState() =>
      _SliverAppMultiselectBarState();
}

class _SliverAppMultiselectBarState extends State<SliverAppMultiselectBar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions = [];

    if (this.widget.selectedElements == 1) {
      actions.add(
        IconButton(
          icon: const Icon(Icons.edit),
          onPressed: () {
            if (widget.onEdit != null) {
              widget.onEdit!();
            }
          },
        ),
      );
    }

    actions.add(
      IconButton(
        icon: const Icon(Icons.delete),
        onPressed: () {
          if (widget.onDelete != null) {
            widget.onDelete!();
          }
        },
      ),
    );

    return SliverAppBar(
      backgroundColor: Colors.white,
      pinned: true,
      actions: actions,
    );
  }
}
