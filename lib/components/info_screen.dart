import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import './alert_info.dart';

class InfoScreen extends StatelessWidget {
  final String? text;
  final List<Widget> buttons;

  InfoScreen({this.text, this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          const SizedBox(height: 32.0),
          AlertInfo(
            text: text,
          ),
          const SizedBox(height: 8.0),
          Row(
            children: buttons,
          ),
        ],
      ),
    );
  }
}
