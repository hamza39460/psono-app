import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart'
    as overlayLoader;
import 'package:psono/theme.dart';

class Loader {
  static show(context, [overlayColor]) {
    overlayLoader.Loader.show(
      context,
      overlayColor: overlayColor ?? Color(0x99ffffff),
      progressIndicator: CircularProgressIndicator(
        color: Color(0xFF151f2b),
        backgroundColor: primarySwatch.shade500,
      ),
    );
  }

  static get isShown => overlayLoader.Loader.isShown;

  static hide() {
    overlayLoader.Loader.hide();
  }
}
