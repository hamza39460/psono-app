import 'package:flutter/material.dart';
import './btn.dart';

class BtnSuccess extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;

  BtnSuccess({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Btn(
      text: text,
      onPressed: () {
        onPressed!();
      },
      color: Color(0xFF2dbb93),
      textColor: Colors.white,
    );
  }
}
