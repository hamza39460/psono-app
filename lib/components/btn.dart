import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;
  final Color? color;
  final Color textColor;

  Btn(
      {this.text,
      this.onPressed,
      this.color = const Color(0xFFE0E0E0),
      this.textColor = const Color(0xFF333333)});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressed!();
      },
      style: ElevatedButton.styleFrom(
        backgroundColor: color,
        padding: const EdgeInsets.all(12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
      ),
      child: Text(
        text!,
        style: TextStyle(color: textColor),
      ),
    );
  }
}
