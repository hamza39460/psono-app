import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:tuple/tuple.dart';

import 'icons.dart';

class KeyValueList extends StatefulWidget {
  final List? contentKV;

  KeyValueList({
    this.contentKV,
  });

  @override
  _KeyValueListState createState() => _KeyValueListState();
}

class _KeyValueListState extends State<KeyValueList> {
  late FToast _fToast;
  List<TextEditingController> controllers = [];
  List? contentKV;

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);
    setState(() {
      contentKV = this.widget.contentKV;
    });
  }

  @override
  void dispose() {
    for (var i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    for (var i = 0; i < this.widget.contentKV!.length; i++) {
      final keyTextController = TextEditingController(
        text: this.widget.contentKV![i]['key'],
      );
      controllers.add(keyTextController);
      children.add(
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              TextFormField(
                controller: keyTextController,
                onChanged: (text) {
                  this.widget.contentKV![i]['key'] = text;
                },
                decoration: new InputDecoration(
                  contentPadding: const EdgeInsets.fromLTRB(0, 10, 48.0, 10),
                  labelText: FlutterI18n.translate(context, 'KEY'),
                ),
              ),
              new DropdownButtonHideUnderline(
                child: DropdownButton<Tuple2<String, int>>(
                  icon: Icon(component.FontAwesome.ellipsis_v),
                  iconSize: 20,
                  dropdownColor: Colors.white,
                  onChanged: (Tuple2<String, int>? newValue) async {
                    if (newValue!.item1 == 'copy-to-clipboard') {
                      Clipboard.setData(
                        new ClipboardData(
                          text: this.widget.contentKV![i]['key'],
                        ),
                      );
                      _clipboardCopyToast();
                    }
                    if (newValue.item1 == 'delete') {
                      this.widget.contentKV!.removeAt(newValue.item2);
                      setState(() {
                        contentKV = this.widget.contentKV;
                      });
                    }
                  },
                  items: [
                    DropdownMenuItem<Tuple2<String, int>>(
                      value: Tuple2<String, int>("copy-to-clipboard", i),
                      child: Row(children: <Widget>[
                        Icon(
                          component.FontAwesome.clipboard,
                          color: Color(0xFF2dbb93),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 12.0,
                          ),
                          child: Text(
                            FlutterI18n.translate(
                              context,
                              "COPY",
                            ),
                          ),
                        ),
                      ]),
                    ),
                    DropdownMenuItem<Tuple2<String, int>>(
                      value: Tuple2<String, int>("delete", i),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            component.FontAwesome.trash,
                            color: Color(0xFF2dbb93),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 12.0,
                            ),
                            child: Text(
                              FlutterI18n.translate(
                                context,
                                "DELETE",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );

      final valueTextController = TextEditingController(
        text: this.widget.contentKV![i]['value'],
      );
      controllers.add(valueTextController);

      children.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              TextFormField(
                controller: valueTextController,
                onChanged: (text) {
                  this.widget.contentKV![i]['value'] = text;
                },
                decoration: new InputDecoration(
                    contentPadding: const EdgeInsets.fromLTRB(0, 10, 48.0, 10),
                    labelText: FlutterI18n.translate(context, 'VALUE')),
              ),
              new DropdownButtonHideUnderline(
                child: DropdownButton<Tuple2<String, int>>(
                  icon: Icon(component.FontAwesome.ellipsis_v),
                  iconSize: 20,
                  dropdownColor: Colors.white,
                  onChanged: (Tuple2<String, int>? newValue) async {
                    if (newValue!.item1 == 'copy-to-clipboard') {
                      Clipboard.setData(
                        new ClipboardData(
                          text: this.widget.contentKV![i]['value'],
                        ),
                      );
                      _clipboardCopyToast();
                    }
                    if (newValue.item1 == 'delete') {
                      this.widget.contentKV!.removeAt(newValue.item2);
                      setState(() {
                        contentKV = this.widget.contentKV;
                      });
                    }
                  },
                  items: [
                    DropdownMenuItem<Tuple2<String, int>>(
                      value: Tuple2<String, int>("copy-to-clipboard", i),
                      child: Row(children: <Widget>[
                        Icon(
                          component.FontAwesome.clipboard,
                          color: Color(0xFF2dbb93),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 12.0,
                          ),
                          child: Text(
                            FlutterI18n.translate(
                              context,
                              "COPY",
                            ),
                          ),
                        ),
                      ]),
                    ),
                    DropdownMenuItem<Tuple2<String, int>>(
                      value: Tuple2<String, int>("delete", i),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            component.FontAwesome.trash,
                            color: Color(0xFF2dbb93),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 12.0,
                            ),
                            child: Text(
                              FlutterI18n.translate(
                                context,
                                "DELETE",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }

    children.add(
      Padding(
        padding: const EdgeInsets.only(top: 10),
        child: component.Btn(
          onPressed: () async {
            this.widget.contentKV!.add({
              'key': '',
              'value': '',
            });
            setState(() {
              contentKV = this.widget.contentKV;
            });
          },
          text: FlutterI18n.translate(context, "ADD_ENTRY"),
        ),
      ),
    );
    return Row(
      children: <Widget>[
        Expanded(
          flex: 10,
          child: Column(
            children: children,
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
        )
      ],
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF2dbb93),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            FontAwesome.clipboard,
            color: Colors.white,
          ),
          const SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(child: child, top: 110, left: 0, right: 0);
        });
  }
}
