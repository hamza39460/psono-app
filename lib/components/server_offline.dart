import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/theme.dart';

import './alert_info.dart';

class ServerOffline extends StatelessWidget {
  final String? text;
  final List<Widget> buttons;

  ServerOffline({this.text, this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    var loadingMessage;
    var spaceLoadingMessage;
    if (text != null) {
      spaceLoadingMessage = const SizedBox(height: 56.0);
      loadingMessage = Container(
        child: Center(
          child: Text(
            text!,
            style: TextStyle(
              color: primarySwatch.shade500,
            ),
          ),
        ),
      );
    } else {
      spaceLoadingMessage = Container();
      loadingMessage = Container();
    }

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          spaceLoadingMessage,
          loadingMessage,
          const SizedBox(height: 24.0),
//                Text(
//                  FlutterI18n.translate(context, "SERVER_OFFLINE"),
//                  textAlign: TextAlign.left,
//                  style: TextStyle(color: Color(0xFFb1b6c1)),
//                ),
          const SizedBox(height: 8.0),
          AlertInfo(
            text: FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          ),
          const SizedBox(height: 8.0),
          Row(
            children: buttons,
          ),
        ],
      ),
    );
  }
}
