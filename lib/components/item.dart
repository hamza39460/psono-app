import 'package:flutter/material.dart';
import 'package:psono/theme.dart';
import './icons.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/manager_widget.dart' as managerWidget;

class Item extends StatelessWidget {
  final datastoreModel.Item? item;
  final VoidCallback? onPressed;
  final datastoreModel.ItemCallback? onLongPress;
  final Color? color;
  final bool? isShare;
  final bool showSelectBox;
  final bool isSelected;

  Item({
    this.item,
    this.onPressed,
    this.onLongPress,
    this.color,
    this.isShare,
    this.showSelectBox = false,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    Widget icon = Icon(
      managerWidget.itemIcon(item!),
      color: color,
      size: 50,
    );

    if (isShare!) {
      icon = Stack(
        alignment: const Alignment(1.5, 1),
        children: [
          Icon(
            managerWidget.itemIcon(item!),
            color: color,
            size: 50,
          ),
          Stack(
            alignment: const Alignment(0, 0),
            children: [
              Icon(
                FontAwesome.circle,
                color: primarySwatch.shade500,
                size: 30,
              ),
              Icon(
                FontAwesome.users,
                color: color,
                size: 15,
              ),
            ],
          )
        ],
      );
    }

    if (showSelectBox && item!.shareRights!.delete!) {
      List<Widget> children = [
        Icon(
          FontAwesome.square,
          color: primarySwatch.shade500,
          size: 30,
        ),
      ];

      if (isSelected) {
        children.add(
          Icon(
            FontAwesome.check,
            color: color,
            size: 20,
          ),
        );
      }

      icon = Stack(
        alignment: const Alignment(-1.5, 1),
        children: [
          icon,
          Stack(
            alignment: const Alignment(0, 0),
            children: children,
          )
        ],
      );
    }

    return GestureDetector(
      onLongPress: () {
        if (onLongPress != null) {
          onLongPress!(item);
        }
      },
      child: Card(
        child: TextButton(
          onPressed: () {
            onPressed!();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              icon,
              Text(item!.name != null ? item!.name! : '',
                  style: TextStyle(color: color),
                  textAlign: TextAlign.center,
                  maxLines: 3)
            ],
          ),
        ),
      ),
    );
  }
}
