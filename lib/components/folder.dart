import 'package:flutter/material.dart';
import './icons.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/theme.dart';

class Folder extends StatelessWidget {
  final datastoreModel.Folder? folder;
  final VoidCallback? onPressed;
  final datastoreModel.FolderCallback? onLongPress;
  final Color? color;
  final bool? isShare;
  final bool showSelectBox;
  final bool isSelected;

  Folder({
    this.folder,
    this.onPressed,
    this.onLongPress,
    this.color,
    this.isShare,
    this.showSelectBox = false,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    Widget icon = Icon(
      FontAwesome.folder,
      color: color,
      size: 50,
    );

    if (isShare!) {
      icon = Stack(
        alignment: const Alignment(1.5, 1),
        children: [
          Icon(
            FontAwesome.folder,
            color: color,
            size: 50,
          ),
          Stack(
            alignment: const Alignment(0, 0),
            children: [
              Icon(
                FontAwesome.circle,
                color: primarySwatch.shade500,
                size: 30,
              ),
              Icon(
                FontAwesome.users,
                color: color,
                size: 15,
              ),
            ],
          )
        ],
      );
    }

    if (showSelectBox && folder!.shareRights!.delete!) {
      List<Widget> children = [
        Icon(
          FontAwesome.square,
          color: primarySwatch.shade500,
          size: 30,
        ),
      ];

      if (isSelected) {
        children.add(
          Icon(
            FontAwesome.check,
            color: color,
            size: 20,
          ),
        );
      }

      icon = Stack(
        alignment: const Alignment(-1.5, 1),
        children: [
          icon,
          Stack(
            alignment: const Alignment(0, 0),
            children: children,
          )
        ],
      );
    }

    return GestureDetector(
      onLongPress: () {
        if (onLongPress != null) {
          onLongPress!(folder);
        }
      },
      child: Card(
        child: TextButton(
          onPressed: () {
            onPressed!();
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              icon,
              Text(folder!.name!,
                  style: TextStyle(color: color),
                  textAlign: TextAlign.center,
                  maxLines: 3)
            ],
          ),
        ),
      ),
    );
  }
}
