import 'package:flutter/material.dart';
import './btn.dart';
import 'package:psono/theme.dart';

class BtnPrimary extends StatelessWidget {
  final String? text;
  final VoidCallback? onPressed;

  BtnPrimary({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Btn(
      text: text,
      onPressed: () {
        onPressed!();
      },
      color: primarySwatch.shade500,
      textColor: Colors.white,
    );
  }
}
