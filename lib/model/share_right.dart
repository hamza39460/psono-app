import 'package:json_annotation/json_annotation.dart';

part 'share_right.g.dart';

/// Represents a datastore
@JsonSerializable()
class ShareRight {
  ShareRight({
    this.read,
    this.write,
    this.grant,
    this.delete,
  });

  bool? read;
  bool? write;
  bool? grant;
  bool? delete;

  ShareRight clone() {
    ShareRight newShareRight = new ShareRight(
      read: this.read,
      write: this.write,
      grant: this.grant,
      delete: this.delete,
    );

    return newShareRight;
  }

  factory ShareRight.fromJson(Map<String, dynamic> json) =>
      _$ShareRightFromJson(json);

  Map<String, dynamic> toJson() => _$ShareRightToJson(this);
}
