import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';

part 'login_result_decrypted.g.dart';

@JsonSerializable()
class Policies {
  Policies({
    this.complianceEnforce2fa,
    this.complianceDisableExport,
    this.complianceDisableDeleteAccount,
    this.complianceDisableOfflineMode,
    this.complianceMaxOfflineCacheTimeValid,
    this.complianceDisableApiKeys,
    this.complianceDisableEmergencyCodes,
    this.complianceDisableRecoveryCodes,
    this.complianceDisableFileRepositories,
    this.complianceDisableLinkShares,
    this.allowedSecondFactors,
    this.allowUserSearchByEmail,
    this.allowUserSearchByUsernamePartial,
  });

  @JsonKey(name: 'compliance_enforce_2fa')
  bool? complianceEnforce2fa;
  @JsonKey(name: 'compliance_disable_export')
  bool? complianceDisableExport;
  @JsonKey(name: 'compliance_disable_delete_account')
  bool? complianceDisableDeleteAccount;
  @JsonKey(name: 'compliance_disable_offline_mode')
  bool? complianceDisableOfflineMode;
  @JsonKey(name: 'compliance_max_offline_cache_time_valid')
  int? complianceMaxOfflineCacheTimeValid;
  @JsonKey(name: 'compliance_disable_api_keys')
  bool? complianceDisableApiKeys;
  @JsonKey(name: 'compliance_disable_emergency_codes')
  bool? complianceDisableEmergencyCodes;
  @JsonKey(name: 'compliance_disable_recovery_codes')
  bool? complianceDisableRecoveryCodes;
  @JsonKey(name: 'compliance_disable_file_repositories')
  bool? complianceDisableFileRepositories;
  @JsonKey(name: 'compliance_disable_link_shares')
  bool? complianceDisableLinkShares;

  @JsonKey(name: 'allowed_second_factors')
  List<String>? allowedSecondFactors;
  @JsonKey(name: 'allow_user_search_by_email')
  bool? allowUserSearchByEmail;
  @JsonKey(name: 'allow_user_search_by_username_partial')
  bool? allowUserSearchByUsernamePartial;

  factory Policies.fromJson(Map<String, dynamic> json) =>
      _$PoliciesFromJson(json);

  Map<String, dynamic> toJson() => _$PoliciesToJson(this);
}

@JsonSerializable()
class User {
  User({
    this.username,
    this.publicKey,
    this.privateKey,
    this.privateKeyNonce,
    this.userSauce,
    this.policies,
  });

  final String? username;
  @JsonKey(name: 'public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? publicKey;
  @JsonKey(name: 'private_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? privateKey;
  @JsonKey(name: 'private_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? privateKeyNonce;
  @JsonKey(name: 'user_sauce')
  final String? userSauce;
  final Policies? policies;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class LoginResultDecrypted {
  LoginResultDecrypted({
    this.token,
    this.requiredMultifactors,
    this.sessionPublicKey,
    this.sessionSecretKey,
    this.sessionSecretKeyNonce,
    this.password,
    this.userValidator,
    this.userValidatorNonce,
    this.user,
  });

  final String? token;
  @JsonKey(name: 'required_multifactors')
  final List<String>? requiredMultifactors;
  @JsonKey(name: 'session_public_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? sessionPublicKey;
  @JsonKey(name: 'session_secret_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? sessionSecretKey;
  @JsonKey(name: 'session_secret_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? sessionSecretKeyNonce;
  final String? password;
  @JsonKey(name: 'user_validator', fromJson: fromHex, toJson: toHex)
  final Uint8List? userValidator;
  @JsonKey(name: 'user_validator_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? userValidatorNonce;
  final User? user;

  factory LoginResultDecrypted.fromJson(Map<String, dynamic> json) =>
      _$LoginResultDecryptedFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResultDecryptedToJson(this);
}
