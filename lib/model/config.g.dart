// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BackendServer _$BackendServerFromJson(Map<String, dynamic> json) =>
    BackendServer(
      title: json['title'] as String?,
      url: json['url'] as String?,
      domain: json['domain'] as String?,
    );

Map<String, dynamic> _$BackendServerToJson(BackendServer instance) =>
    <String, dynamic>{
      'title': instance.title,
      'url': instance.url,
      'domain': instance.domain,
    };

SamlProvider _$SamlProviderFromJson(Map<String, dynamic> json) => SamlProvider(
      title: json['title'] as String?,
      providerId: json['provider_id'] as int?,
      buttonName: json['button_name'] as String?,
    );

Map<String, dynamic> _$SamlProviderToJson(SamlProvider instance) =>
    <String, dynamic>{
      'title': instance.title,
      'provider_id': instance.providerId,
      'button_name': instance.buttonName,
    };

OidcProvider _$OidcProviderFromJson(Map<String, dynamic> json) => OidcProvider(
      title: json['title'] as String?,
      providerId: json['provider_id'] as int?,
      buttonName: json['button_name'] as String?,
    );

Map<String, dynamic> _$OidcProviderToJson(OidcProvider instance) =>
    <String, dynamic>{
      'title': instance.title,
      'provider_id': instance.providerId,
      'button_name': instance.buttonName,
    };

MoreLink _$MoreLinkFromJson(Map<String, dynamic> json) => MoreLink(
      href: json['href'] as String?,
      title: json['title'] as String?,
      className: json['class'] as String?,
    );

Map<String, dynamic> _$MoreLinkToJson(MoreLink instance) => <String, dynamic>{
      'href': instance.href,
      'title': instance.title,
      'class': instance.className,
    };

ConfigJson _$ConfigJsonFromJson(Map<String, dynamic> json) => ConfigJson(
      allowCustomServer: json['allow_custom_server'] as bool?,
      allowRegistration: json['allow_registration'] as bool?,
      allowLostPassword: json['allow_lost_password'] as bool?,
      authenticationMethods: (json['authentication_methods'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      baseUrl: json['base_url'] as String?,
      moreLinks: (json['more_links'] as List<dynamic>?)
          ?.map((e) => MoreLink.fromJson(e as Map<String, dynamic>))
          .toList(),
      backendServers: (json['backend_servers'] as List<dynamic>?)
          ?.map((e) => BackendServer.fromJson(e as Map<String, dynamic>))
          .toList(),
      samlProvider: (json['saml_provider'] as List<dynamic>?)
          ?.map((e) => SamlProvider.fromJson(e as Map<String, dynamic>))
          .toList(),
      oidcProvider: (json['oidc_provider'] as List<dynamic>?)
          ?.map((e) => OidcProvider.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ConfigJsonToJson(ConfigJson instance) =>
    <String, dynamic>{
      'allow_custom_server': instance.allowCustomServer,
      'allow_registration': instance.allowRegistration,
      'allow_lost_password': instance.allowLostPassword,
      'authentication_methods': instance.authenticationMethods,
      'base_url': instance.baseUrl,
      'more_links': instance.moreLinks,
      'backend_servers': instance.backendServers,
      'saml_provider': instance.samlProvider,
      'oidc_provider': instance.oidcProvider,
    };

ConfigV2 _$ConfigV2FromJson(Map<String, dynamic> json) => ConfigV2(
      verifyKey: fromHex(json['verify_key'] as String?),
      url: json['url'] as String?,
    );

Map<String, dynamic> _$ConfigV2ToJson(ConfigV2 instance) => <String, dynamic>{
      'verify_key': toHex(instance.verifyKey),
      'url': instance.url,
    };

Config _$ConfigFromJson(Map<String, dynamic> json) => Config(
      version: json['version'] as int? ?? 1,
      configJson: json['ConfigJson'] == null
          ? null
          : ConfigJson.fromJson(json['ConfigJson'] as Map<String, dynamic>),
      config: json['config'] == null
          ? null
          : ConfigV2.fromJson(json['config'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfigToJson(Config instance) => <String, dynamic>{
      'ConfigJson': instance.configJson,
      'version': instance.version,
      'config': instance.config,
    };
