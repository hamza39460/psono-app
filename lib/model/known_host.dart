import 'dart:typed_data';
import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';

part 'known_host.g.dart';

@JsonSerializable()
class KnownHost {
  KnownHost({
    this.url,
    this.verifyKey,
  });

  final String? url;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  Uint8List? verifyKey;

  factory KnownHost.fromJson(Map<String, dynamic> json) =>
      _$KnownHostFromJson(json);

  Map<String, dynamic> toJson() => _$KnownHostToJson(this);
}
