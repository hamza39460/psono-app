// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_info_decrypted.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInfoDecrypted _$LoginInfoDecryptedFromJson(Map<String, dynamic> json) =>
    LoginInfoDecrypted(
      data: fromHex(json['data'] as String?),
      dataNonce: fromHex(json['data_nonce'] as String?),
      serverSessionPublicKey:
          fromHex(json['server_session_public_key'] as String?),
    );

Map<String, dynamic> _$LoginInfoDecryptedToJson(LoginInfoDecrypted instance) =>
    <String, dynamic>{
      'data': toHex(instance.data),
      'data_nonce': toHex(instance.dataNonce),
      'server_session_public_key': toHex(instance.serverSessionPublicKey),
    };
