import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:psono/model/config.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/model/login_info.dart';
import 'package:psono/model/login_info_decrypted.dart';
import 'package:psono/model/login_result_decrypted.dart';
import 'package:psono/model/public_private_key_pair.dart';
import 'package:psono/model/recovery_enable.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/device.dart' as device;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;
import 'package:psono/services/offline_cache.dart' as offlineCache;
import 'package:psono/services/storage.dart';

String? _sessionPassword;
String? _userSauce;
String? _sessionToken;
EncryptedData? _sessionVerification;
Uint8List? _sessionSecretKey;
Uint8List? _userPublicKey;
Uint8List? _userPrivateKey;
List<String>? _requiredMultifactors;

/// Checks if the user is logged in.
isLoggedIn() {
  return reduxStore.state.token != null && reduxStore.state.token != "";
}

/// Handles the validation of the token with the server by solving the cryptographic puzzle
/// Returns a promise with the the final activate token was successful or not
Future activateToken() async {
  String? password = _sessionPassword;
  String userSauce = _userSauce!;
  Uint8List? publicKey = _userPublicKey;
  String token = _sessionToken!;
  EncryptedData verification = _sessionVerification!;
  Uint8List? sessionSecretKey = _sessionSecretKey;
  Uint8List? privateKey = _userPrivateKey;

  var activateTokenResponse = await apiClient.activateToken(
    token,
    verification.text,
    verification.nonce,
    sessionSecretKey,
  );

  Uint8List? secretKey = converter.fromHex(
    await cryptoLibrary.decryptSecret(
      activateTokenResponse.user!.secretKey!,
      activateTokenResponse.user!.secretKeyNonce!,
      password,
      userSauce,
    ),
  );

  await storage.write(
    key: 'token',
    value: token,
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'sessionSecretKey',
    value: converter.toHex(sessionSecretKey),
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'secretKey',
    value: converter.toHex(secretKey),
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'publicKey',
    value: converter.toHex(publicKey),
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'privateKey',
    value: converter.toHex(privateKey),
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'userId',
    value: activateTokenResponse.user!.id,
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'userEmail',
    value: activateTokenResponse.user!.email,
    iOptions: secureIOSOptions,
  );
  await storage.write(
    key: 'userSauce',
    value: userSauce,
    iOptions: secureIOSOptions,
  );

  reduxStore.dispatch(
    SetSessionInfoAction(
      activateTokenResponse.user!.id,
      activateTokenResponse.user!.email,
      token,
      sessionSecretKey,
      secretKey,
      publicKey,
      privateKey,
      userSauce,
    ),
  );

  Timer(Duration(milliseconds: 2000), () {
    offlineCache.incrementalUpdate(ratelimit: false);
  });

  _sessionPassword = null;
  _userSauce = null;
  _sessionToken = null;
  _sessionVerification = null;
  _sessionSecretKey = null;
  _userPublicKey = null;
  _userPrivateKey = null;
}

/// Ajax POST request to the backend with the token [yubikeyOtp].
/// Returns a promise with the login status
Future yubikeyOtpVerify(yubikeyOtp) async {
  await apiClient.yubikeyOtpVerify(
    _sessionToken!,
    _sessionSecretKey,
    yubikeyOtp,
  );

  _requiredMultifactors!.remove('yubikey_otp_2fa');

  return _requiredMultifactors;
}

/// Ajax POST request to the backend with the token [gaToken].
/// Returns a promise with the login status
Future gaVerify(gaToken) async {
  await apiClient.gaVerify(
    _sessionToken!,
    _sessionSecretKey,
    gaToken,
  );

  _requiredMultifactors!.remove('google_authenticator_2fa');

  return _requiredMultifactors;
}

/// Ajax POST request to the backend with the token [duoToken].
/// Returns a promise with the login status
Future duoVerify(duoToken) async {
  await apiClient.duoVerify(
    _sessionToken!,
    _sessionSecretKey,
    duoToken,
  );

  _requiredMultifactors!.remove('duo_2fa');

  return _requiredMultifactors;
}

/// Ajax POST request to the backend with [username] and authkey for login, saves a token together with user_id
/// and all the different keys of a user in the api data storage.
/// Also handles the validation of the token with the server by solving the cryptographic puzzle
Future<List<String>?> login(
  String username,
  String? domain,
  String password,
  apiClient.Info info,
  bool sendPlain,
) async {
  username = helper.formFullUsername(username, domain);
  final String authkey = await cryptoLibrary.generateAuthkey(
    username,
    password,
  );
  final PublicPrivateKeyPair sessionKeyPair =
      await cryptoLibrary.generatePublicPrivateKeypair();
  final String? deviceFingerprint = await device.getDeviceFingerprint();
  final String? deviceDescription = await device.getDeviceDescription();

  String? _password;
  if (sendPlain) {
    _password = password;
  }
  _sessionPassword = password;

  final LoginInfo loginInfo = LoginInfo(
    username: username,
    authkey: authkey,
    deviceTime: new DateTime.now(),
    deviceFingerprint: deviceFingerprint,
    deviceDescription: deviceDescription,
    password: _password,
    clientType: 'app',
  );

  final String loginInfoString = jsonEncode(loginInfo);

  final EncryptedData loginInfoEnc = await cryptoLibrary.encryptDataPublicKey(
    loginInfoString,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  int sessionDuration = 24 * 60 * 60 * 30;

  apiClient.Login loginResult = await apiClient.login(
    loginInfoEnc.text,
    loginInfoEnc.nonce,
    sessionKeyPair.publicKey,
    sessionDuration,
  );

  String loginResultJson = await cryptoLibrary.decryptDataPublicKey(
    loginResult.loginInfo!,
    loginResult.loginInfoNonce!,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  LoginResultDecrypted loginResultDecrypted = LoginResultDecrypted.fromJson(
    jsonDecode(loginResultJson),
  );
  _userSauce = loginResultDecrypted.user!.userSauce;
  _userPublicKey = loginResultDecrypted.user!.publicKey;

  if (loginResultDecrypted.user!.policies != null) {
    var complianceDisableDeleteAccount =
        loginResultDecrypted.user!.policies!.complianceDisableDeleteAccount ??
            reduxStore.state.complianceDisableDeleteAccount;
    var complianceDisableOfflineMode =
        loginResultDecrypted.user!.policies!.complianceDisableOfflineMode ??
            reduxStore.state.complianceDisableOfflineMode;
    var complianceMaxOfflineCacheTimeValid = loginResultDecrypted
            .user!.policies!.complianceMaxOfflineCacheTimeValid ??
        reduxStore.state.complianceMaxOfflineCacheTimeValid;
    reduxStore.dispatch(
      SetServerPolicyAction(
        complianceDisableDeleteAccount,
        complianceDisableOfflineMode,
        complianceMaxOfflineCacheTimeValid,
      ),
    );

    await storage.write(
      key: 'complianceDisableDeleteAccount',
      value: complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceDisableOfflineMode',
      value: complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceMaxOfflineCacheTimeValid',
      value: complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );
  }

  _sessionSecretKey = converter.fromHex(
    await cryptoLibrary.decryptDataPublicKey(
      loginResultDecrypted.sessionSecretKey!,
      loginResultDecrypted.sessionSecretKeyNonce!,
      loginResultDecrypted.sessionPublicKey!,
      sessionKeyPair.privateKey,
    ),
  );

  Uint8List userPrivateKey = converter.fromHex(
    await cryptoLibrary.decryptSecret(
      loginResultDecrypted.user!.privateKey!,
      loginResultDecrypted.user!.privateKeyNonce!,
      password,
      loginResultDecrypted.user!.userSauce!,
    ),
  )!;
  _userPrivateKey = userPrivateKey;

  String userValidator = await cryptoLibrary.decryptDataPublicKey(
    loginResultDecrypted.userValidator!,
    loginResultDecrypted.userValidatorNonce!,
    loginResultDecrypted.sessionPublicKey!,
    userPrivateKey,
  );

  _sessionToken = loginResultDecrypted.token;

  _sessionVerification = await cryptoLibrary.encryptData(
    userValidator,
    _sessionSecretKey!,
  );

  _requiredMultifactors = loginResultDecrypted.requiredMultifactors;

  return _requiredMultifactors;
}

String getSamlReturnToUrl() {
  return 'https://psono.com/redirect-app#!/saml/token/';
}

String getOidcReturnToUrl() {
  return 'https://psono.com/redirect-app#!/oidc/token/';
}

/// A function that launches the web auth flow and returns the samlTokenId
Future<String> launchWebAuthFlow(
  BuildContext context,
  String redirectUrl,
  Size screenSize,
  String returnToUrl,
) async {
  // Present the dialog to the user
  final url = await FlutterWebAuth.authenticate(
      url: redirectUrl, callbackUrlScheme: 'com.psono.psono');
  String tokenId = url.replaceAll(
      returnToUrl.replaceAll('https://', 'com.psono.psono://'), '');
  return tokenId;
}

/// A function that handles saml login intitiation
Future<String> samlInitiateLogin(
  BuildContext context,
  SamlProvider provider,
  Size screenSize,
) async {
  String returnToUrl = getSamlReturnToUrl();

  apiClient.SamlInitiateLogin loginResult = await apiClient.samlInitiateLogin(
    provider.providerId,
    returnToUrl,
  );

  String samlTokenId = await launchWebAuthFlow(
      context, loginResult.samlRedirectUrl!, screenSize, returnToUrl);

  return samlTokenId;
}

/// Will try to use the token to authenticate and login.
Future<List<String>?> samlLogin(
  String samlTokenId,
  apiClient.Info info,
) async {
  final PublicPrivateKeyPair sessionKeyPair =
      await cryptoLibrary.generatePublicPrivateKeypair();
  final String? deviceFingerprint = await device.getDeviceFingerprint();
  final String? deviceDescription = await device.getDeviceDescription();

  final LoginInfo loginInfo = LoginInfo(
    samlTokenId: samlTokenId,
    deviceTime: new DateTime.now(),
    deviceFingerprint: deviceFingerprint,
    deviceDescription: deviceDescription,
  );

  final String loginInfoString = jsonEncode(loginInfo);

  final EncryptedData loginInfoEnc = await cryptoLibrary.encryptDataPublicKey(
    loginInfoString,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  int sessionDuration = 24 * 60 * 60;

  apiClient.SamlLogin loginResult = await apiClient.samlLogin(
    loginInfoEnc.text,
    loginInfoEnc.nonce,
    sessionKeyPair.publicKey,
    sessionDuration,
  );

  String loginInfoDecryptedJson = await cryptoLibrary.decryptDataPublicKey(
    loginResult.loginInfo!,
    loginResult.loginInfoNonce!,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  LoginInfoDecrypted loginInfoDecrypted = LoginInfoDecrypted.fromJson(
    jsonDecode(loginInfoDecryptedJson),
  );

  String loginDataDecryptedJson = await cryptoLibrary.decryptDataPublicKey(
    loginInfoDecrypted.data!,
    loginInfoDecrypted.dataNonce!,
    loginInfoDecrypted.serverSessionPublicKey!,
    sessionKeyPair.privateKey,
  );

  LoginResultDecrypted loginResultDecrypted = LoginResultDecrypted.fromJson(
    jsonDecode(loginDataDecryptedJson),
  );

  if (loginResultDecrypted.user!.policies != null) {
    var complianceDisableDeleteAccount =
        loginResultDecrypted.user!.policies!.complianceDisableDeleteAccount ??
            reduxStore.state.complianceDisableDeleteAccount;
    var complianceDisableOfflineMode =
        loginResultDecrypted.user!.policies!.complianceDisableOfflineMode ??
            reduxStore.state.complianceDisableOfflineMode;
    var complianceMaxOfflineCacheTimeValid = loginResultDecrypted
            .user!.policies!.complianceMaxOfflineCacheTimeValid ??
        reduxStore.state.complianceMaxOfflineCacheTimeValid;
    reduxStore.dispatch(
      SetServerPolicyAction(
        complianceDisableDeleteAccount,
        complianceDisableOfflineMode,
        complianceMaxOfflineCacheTimeValid,
      ),
    );

    await storage.write(
      key: 'complianceDisableDeleteAccount',
      value: complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceDisableOfflineMode',
      value: complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceMaxOfflineCacheTimeValid',
      value: complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );
  }

  _userSauce = loginResultDecrypted.user!.userSauce;
  _userPublicKey = loginResultDecrypted.user!.publicKey;
  _sessionSecretKey = loginResultDecrypted.sessionSecretKey;
  _sessionPassword = loginResultDecrypted.password;

  Uint8List userPrivateKey = converter.fromHex(
    await cryptoLibrary.decryptSecret(
      loginResultDecrypted.user!.privateKey!,
      loginResultDecrypted.user!.privateKeyNonce!,
      loginResultDecrypted.password,
      loginResultDecrypted.user!.userSauce!,
    ),
  )!;
  _userPrivateKey = userPrivateKey;

  String userValidator = await cryptoLibrary.decryptDataPublicKey(
    loginResultDecrypted.userValidator!,
    loginResultDecrypted.userValidatorNonce!,
    loginInfoDecrypted.serverSessionPublicKey!,
    userPrivateKey,
  );

  _sessionToken = loginResultDecrypted.token;

  _sessionVerification = await cryptoLibrary.encryptData(
    userValidator,
    _sessionSecretKey!,
  );

  _requiredMultifactors = loginResultDecrypted.requiredMultifactors;

  reduxStore.dispatch(
    SetUserUsernameAction(
      loginResultDecrypted.user!.username,
    ),
  );

  return _requiredMultifactors;
}

/// A function that handles oidc login intitiation
Future<String> oidcInitiateLogin(
  BuildContext context,
  OidcProvider provider,
  Size screenSize,
) async {
  String returnToUrl = getOidcReturnToUrl();

  apiClient.OidcInitiateLogin loginResult = await apiClient.oidcInitiateLogin(
    provider.providerId,
    returnToUrl,
  );
  String oidcTokenId = await launchWebAuthFlow(
      context, loginResult.oidcRedirectUrl!, screenSize, returnToUrl);

  return oidcTokenId;
}

/// Will try to use the token to authenticate and login.
Future<List<String>?> oidcLogin(
  String oidcTokenId,
  apiClient.Info info,
) async {
  final PublicPrivateKeyPair sessionKeyPair =
      await cryptoLibrary.generatePublicPrivateKeypair();
  final String? deviceFingerprint = await device.getDeviceFingerprint();
  final String? deviceDescription = await device.getDeviceDescription();

  final LoginInfo loginInfo = LoginInfo(
    oidcTokenId: oidcTokenId,
    deviceTime: new DateTime.now(),
    deviceFingerprint: deviceFingerprint,
    deviceDescription: deviceDescription,
  );

  final String loginInfoString = jsonEncode(loginInfo);

  final EncryptedData loginInfoEnc = await cryptoLibrary.encryptDataPublicKey(
    loginInfoString,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  int sessionDuration = 24 * 60 * 60;

  apiClient.OidcLogin loginResult = await apiClient.oidcLogin(
    loginInfoEnc.text,
    loginInfoEnc.nonce,
    sessionKeyPair.publicKey,
    sessionDuration,
  );

  String loginInfoDecryptedJson = await cryptoLibrary.decryptDataPublicKey(
    loginResult.loginInfo!,
    loginResult.loginInfoNonce!,
    info.publicKey!,
    sessionKeyPair.privateKey,
  );

  LoginInfoDecrypted loginInfoDecrypted = LoginInfoDecrypted.fromJson(
    jsonDecode(loginInfoDecryptedJson),
  );

  String loginDataDecryptedJson = await cryptoLibrary.decryptDataPublicKey(
    loginInfoDecrypted.data!,
    loginInfoDecrypted.dataNonce!,
    loginInfoDecrypted.serverSessionPublicKey!,
    sessionKeyPair.privateKey,
  );

  LoginResultDecrypted loginResultDecrypted = LoginResultDecrypted.fromJson(
    jsonDecode(loginDataDecryptedJson),
  );

  if (loginResultDecrypted.user!.policies != null) {
    var complianceDisableDeleteAccount =
        loginResultDecrypted.user!.policies!.complianceDisableDeleteAccount ??
            reduxStore.state.complianceDisableDeleteAccount;
    var complianceDisableOfflineMode =
        loginResultDecrypted.user!.policies!.complianceDisableOfflineMode ??
            reduxStore.state.complianceDisableOfflineMode;
    var complianceMaxOfflineCacheTimeValid = loginResultDecrypted
            .user!.policies!.complianceMaxOfflineCacheTimeValid ??
        reduxStore.state.complianceMaxOfflineCacheTimeValid;
    reduxStore.dispatch(
      SetServerPolicyAction(
        complianceDisableDeleteAccount,
        complianceDisableOfflineMode,
        complianceMaxOfflineCacheTimeValid,
      ),
    );

    await storage.write(
      key: 'complianceDisableDeleteAccount',
      value: complianceDisableDeleteAccount.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceDisableOfflineMode',
      value: complianceDisableOfflineMode.toString(),
      iOptions: secureIOSOptions,
    );

    await storage.write(
      key: 'complianceMaxOfflineCacheTimeValid',
      value: complianceMaxOfflineCacheTimeValid.toString(),
      iOptions: secureIOSOptions,
    );
  }

  _userSauce = loginResultDecrypted.user!.userSauce;
  _userPublicKey = loginResultDecrypted.user!.publicKey;
  _sessionSecretKey = loginResultDecrypted.sessionSecretKey;
  _sessionPassword = loginResultDecrypted.password;

  Uint8List userPrivateKey = converter.fromHex(
    await cryptoLibrary.decryptSecret(
      loginResultDecrypted.user!.privateKey!,
      loginResultDecrypted.user!.privateKeyNonce!,
      loginResultDecrypted.password,
      loginResultDecrypted.user!.userSauce!,
    ),
  )!;
  _userPrivateKey = userPrivateKey;

  String userValidator = await cryptoLibrary.decryptDataPublicKey(
    loginResultDecrypted.userValidator!,
    loginResultDecrypted.userValidatorNonce!,
    loginInfoDecrypted.serverSessionPublicKey!,
    userPrivateKey,
  );

  _sessionToken = loginResultDecrypted.token;

  _sessionVerification = await cryptoLibrary.encryptData(
    userValidator,
    _sessionSecretKey!,
  );

  _requiredMultifactors = loginResultDecrypted.requiredMultifactors;

  reduxStore.dispatch(
    SetUserUsernameAction(
      loginResultDecrypted.user!.username,
    ),
  );

  return _requiredMultifactors;
}

/// Logs the user out. Destroys the session on the backend and deletes all local
/// session information.
Future logout() async {
  try {
    await apiClient.logout(
        reduxStore.state.token, reduxStore.state.sessionSecretKey, null);
  } catch (e) {
    // pass
  }

  await storage.delete(key: 'token');
  await storage.delete(key: 'sessionSecretKey');
  await storage.delete(key: 'secretKey');
  await storage.delete(key: 'privateKey');

  reduxStore.dispatch(
    SetSessionInfoAction(
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      null,
    ),
  );

  offlineCache.wipe();

  managerDatastore.logout();
}

/// Initiate the password reset
Future<RecoveryEnable> recoveryEnable(
  String username,
  String recoveryCode,
  String server,
) async {
  String recoveryAuthkey = await cryptoLibrary.generateAuthkey(
    username,
    recoveryCode,
  );

  apiClient.EnableRecoverycode data = await apiClient.enableRecoverycode(
    username,
    recoveryAuthkey,
  );

  Map recoveryData = jsonDecode(
    await cryptoLibrary.decryptSecret(
      data.recoveryData!,
      data.recoveryDataNonce!,
      recoveryCode,
      data.recoverySauce!,
    ),
  );

  return RecoveryEnable(
    converter.fromHex(recoveryData['user_private_key'] as String?),
    converter.fromHex(recoveryData['user_secret_key'] as String?),
    data.userSauce,
    data.verifierPublicKey,
    data.verifierTimeValid,
  );
}

/// Encrypts the recovered data with the new password and initiates the save of this data
Future setPassword(
  String username,
  String recoveryCode,
  String password,
  Uint8List userPrivateKey,
  Uint8List? userSecretKey,
  String userSauce,
  Uint8List verifierPublicKey,
) async {
  EncryptedData privKeyEnc = await cryptoLibrary.encryptSecret(
    converter.toHex(userPrivateKey)!,
    password,
    userSauce,
  );
  EncryptedData secretKeyEnc = await cryptoLibrary.encryptSecret(
    converter.toHex(userSecretKey)!,
    password,
    userSauce,
  );

  String updateRequest = jsonEncode({
    'authkey': await cryptoLibrary.generateAuthkey(username, password),
    'private_key': converter.toHex(privKeyEnc.text),
    'private_key_nonce': converter.toHex(privKeyEnc.nonce),
    'secret_key': converter.toHex(secretKeyEnc.text),
    'secret_key_nonce': converter.toHex(secretKeyEnc.nonce)
  });

  EncryptedData updateRequestEnc = await cryptoLibrary.encryptDataPublicKey(
    updateRequest,
    verifierPublicKey,
    userPrivateKey,
  );

  String recoveryAuthkey = await cryptoLibrary.generateAuthkey(
    username,
    recoveryCode,
  );

  return await apiClient.setPassword(
    username,
    recoveryAuthkey,
    updateRequestEnc.text,
    updateRequestEnc.nonce,
  );
}

Future<void> deleteAccount(String password) async {
  var authkey = await cryptoLibrary.generateAuthkey(
    reduxStore.state.username,
    password,
  );

  await apiClient.deleteAccount(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    authkey,
  );

  try {
    await logout();
  } catch (e) {
    // pass
  }
}

/// Responsible for the registration. Generates the users public-private-key-pair together with the secret
/// key and the user sauce. Encrypts the sensible data before initiating the register call with the api client.
Future<void> register(
  String email,
  String username,
  String password,
  String? server,
) async {
  final PublicPrivateKeyPair pair =
      await cryptoLibrary.generatePublicPrivateKeypair();
  final String userSauce = await cryptoLibrary.generateUserSauce();

  EncryptedData privKeyEnc = await cryptoLibrary.encryptSecret(
    converter.toHex(
      pair.privateKey,
    )!,
    password,
    userSauce,
  );
  EncryptedData secretKeyEnc = await cryptoLibrary.encryptSecret(
    converter.toHex(
      await cryptoLibrary.generateSecretKey(),
    )!,
    password,
    userSauce,
  );

  String baseUrl = 'https://psono.com/activate-registration';

  String authkey = await cryptoLibrary.generateAuthkey(username, password);

  await apiClient.register(
    email,
    username,
    authkey,
    pair.publicKey,
    privKeyEnc.text,
    privKeyEnc.nonce,
    secretKeyEnc.text,
    secretKeyEnc.nonce,
    userSauce,
    baseUrl,
  );
}

/// Update user base settings
Future<void> updateUser(
  String? email,
  String? authkey,
  String authkeyOld,
  Uint8List? privateKey,
  Uint8List? privateKeyNonce,
  Uint8List? secretKey,
  Uint8List? secretKeyNonce,
) async {
  return await apiClient.updateUser(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    email,
    authkey,
    authkeyOld,
    privateKey,
    privateKeyNonce,
    secretKey,
    secretKeyNonce,
  );
}

/// Saves a new email
Future<void> saveNewEmail(String email, String password) async {
  String authkey = await cryptoLibrary.generateAuthkey(
    reduxStore.state.username,
    password,
  );

  await updateUser(
    email,
    null,
    authkey,
    null,
    null,
    null,
    null,
  );

  reduxStore.dispatch(
    SetUserEmailAction(
      email,
    ),
  );
  await storage.write(
      key: 'userEmail', value: email, iOptions: secureIOSOptions);
}

/// Saves a new email
Future<void> saveNewPassword(
    String newPassword, String newPasswordRepeat, String oldPassword) async {
  String oldAuthkey = await cryptoLibrary.generateAuthkey(
    reduxStore.state.username,
    oldPassword,
  );
  String newAuthkey = await cryptoLibrary.generateAuthkey(
    reduxStore.state.username,
    newPassword,
  );

  EncryptedData privKeyEnc = await cryptoLibrary.encryptSecret(
      converter.toHex(reduxStore.state.privateKey)!,
      newPassword,
      reduxStore.state.userSauce);
  EncryptedData secretKeyEnc = await cryptoLibrary.encryptSecret(
      converter.toHex(reduxStore.state.secretKey)!,
      newPassword,
      reduxStore.state.userSauce);

  await updateUser(
    null,
    newAuthkey,
    oldAuthkey,
    privKeyEnc.text,
    privKeyEnc.nonce,
    secretKeyEnc.text,
    secretKeyEnc.nonce,
  );
}
