import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/services/manager_secret.dart' as managerSecret;

const MethodChannel channel =
    MethodChannel('com.psono.psono/psono_autofill_service');

/// Checks whether the autofill service is supported
Future<bool> isSupported() async {
  if (Platform.isIOS) {
    return true;
  }

  if (Platform.isAndroid) {
    bool hasAutofillServicesSupport = await channel.invokeMethod('isSupported');
    return hasAutofillServicesSupport;
  }

  return false;
}

/// Checks whether the autofill service is enabled
Future<bool?> isEnabled() async {
  if (!Platform.isAndroid) {
    return false;
  }

  bool? isEnabledResult = await channel.invokeMethod('isEnabled');

  return isEnabledResult;
}

/// Enables the autofill service
Future<bool?> enable() async {
  if (!Platform.isAndroid) {
    return false;
  }

  bool? enableResult = await channel.invokeMethod('enable');

  return enableResult;
}

/// Disables the autofill service
Future<bool?> disable() async {
  if (!Platform.isAndroid) {
    return false;
  }

  bool? disableResult = await channel.invokeMethod('disable');

  return disableResult;
}

/// Disables the autofill service
Future<bool?> autofill(String? secretId, Uint8List? secretKey) async {
  if (!Platform.isAndroid) {
    return false;
  }

  Secret secret = await managerSecret.readSecret(secretId!, secretKey!);

  bool? autofillResult = await channel.invokeMethod('autofill', secret.data);

  return autofillResult;
}
