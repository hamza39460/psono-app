import 'dart:async';
import 'dart:math';

import 'package:crypto/crypto.dart' as crypto;
import 'package:flutter/foundation.dart';
import 'package:pinenacl/api.dart';
import 'package:pinenacl/ed25519.dart' show VerifyKey, Signature;
import 'package:pinenacl/x25519.dart' show Box, SecretBox;
import "package:pointycastle/api.dart" show Digest;
import "package:pointycastle/digests/sha512.dart";
import "package:pointycastle/key_derivators/api.dart";
import "package:pointycastle/key_derivators/scrypt.dart";
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/model/public_private_key_pair.dart';
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/helper.dart' as helper;
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

// https://github.com/firstfloorsoftware/flutter_sodium/blob/master/example/lib/main.dart

class InvalidRecoveryCodeException implements Exception {
  final String? message;

  InvalidRecoveryCodeException([this.message]);

  String toString() => message!;
}

/// Random byte generator
///
/// @param count The amount of random bytes to return
///
/// @returns Random byte array
Future<Uint8List> randomBytes(int count) async {
  final Random random = Random.secure();

  final Uint8List randomBytesArray = Uint8List.fromList(
    List<int>.generate(
      count,
      (i) => random.nextInt(256),
    ),
  );
  return randomBytesArray;
}

/// Returns a cryptographically secure random number between 0 (included) and 1 (excluded)
///
/// @returns Random number between 0 and 1
Future<double> random() async {
  final Random random = Random.secure();
  return random.nextDouble();
}

/// Returns whether the provided message and verifyKey produce the correct signature or not
///
/// @param message The raw message to verify
/// @param signature The hex representation of the signature
/// @param verify_key The hex representation of the verification key
///
/// @returns Returns whether the signature is correct or not
Future<bool> validateSignature(
    String message, Uint8List signature, Uint8List verifyKey) async {
  final Uint8List messageBytes = converter.encodeUTF8(message);

  try {
    final verificationInstance = VerifyKey(verifyKey);
    return verificationInstance.verify(
      message: messageBytes,
      signature: Signature(signature),
    );
  } catch (e) {
    return false;
  }
}

/// Returns the hex encoded sha256
///
/// @returns The sha256 hash of the data string
Future<String?> sha256(String data) async {
  final Uint8List bytes = converter.encodeUTF8(data);

  var digest = crypto.sha256.convert(bytes);
  final String? sha256Hex = converter.toHex(digest.bytes as Uint8List?);

  return sha256Hex;
}

/// Returns the hex encoded sha512
///
/// @returns The sha512 hash of the data string
Future<String?> sha512(String data) async {
  final Uint8List bytes = converter.encodeUTF8(data);
  final Digest sha512 = new SHA512Digest();
  final String? sha512Hex = converter.toHex(sha512.process(bytes));

  return sha512Hex;
}

/// Generates a public and private key pair
/// All keys are 32 Bytes or 256 Bits long and represented as Uint8 Byte Arrays
Future<PublicPrivateKeyPair> generatePublicPrivateKeypair() async {
  var box = PrivateKey.generate();
  return PublicPrivateKeyPair(
    Uint8List.fromList(box.publicKey),
    Uint8List.fromList(box),
  );
}

/// Takes the data and encrypts that with a random nonce, the receivers public key and users private key.
///
/// @param data The data you want to encrypt
/// @param public_key The public key you want to use for the encryption
/// @param private_key The private key you want to use for the encryption
///
/// @returns The encrypted text and the nonce
Future<EncryptedData> encryptDataPublicKey(
    String data, Uint8List publicKey, Uint8List privateKey) async {
  final Uint8List nonce = await randomBytes(24);
  final Uint8List dataBytes = converter.encodeUTF8(data);
  var box = Box(
    myPrivateKey: PrivateKey(privateKey),
    theirPublicKey: PublicKey(publicKey),
  );

  var text = box.encrypt(dataBytes, nonce: nonce);

  return EncryptedData(Uint8List.fromList(text.cipherText), nonce);
}

/// Takes the cipher text and decrypts that with the nonce, the senders public key and users private key.
///
/// @param text The encrypted text
/// @param nonce The nonce that belongs to the encrypted text
/// @param public_key The pulic key you want to use to decrypt the text
/// @param private_key The private key you want to use to encrypt the text
///
///  @returns The decrypted data
Future<String> decryptDataPublicKey(Uint8List text, Uint8List nonce,
    Uint8List publicKey, Uint8List privateKey) async {
  var box = Box(
    myPrivateKey: PrivateKey(privateKey),
    theirPublicKey: PublicKey(publicKey),
  );

  final Uint8List decryptedBytes = box.decrypt(
    EncryptedMessage(
      nonce: nonce,
      cipherText: text,
    ),
  );
  final String decrypted = converter.decodeUTF8(decryptedBytes);

  return decrypted;
}

/// returns a 32 bytes long random hex value to be used as the user special sauce
Future<String> generateUserSauce() async {
  return converter.toHex(await randomBytes(32))!;
}

/// Takes the secret and encrypts that with the provided password. The crypto_box takes only 256 bits, therefore we
/// are using sha256(password+user_sauce) as key for encryption.
/// Returns the nonce and the cipher text
///
/// @param secret The secret you want to encrypt
/// @param password The password you want to use to encrypt the secret
/// @param user_sauce The user's sauce
///
/// @returns The encrypted text and the nonce
Future<EncryptedData> encryptSecret(
    String secret, String password, String userSauce) async {
  final String? salt = await sha512(userSauce);
  final Uint8List k = converter.fromHex(
    await sha256(await passwordScrypt({'password': password, 'salt': salt})),
  )!;

  final Uint8List nonce = await randomBytes(24);
  final box = SecretBox(k);

  var encryptedData = box.encrypt(
    converter.encodeUTF8(secret),
    nonce: nonce,
  );

  return EncryptedData(Uint8List.fromList(encryptedData.cipherText), nonce);
}

/// Takes the cipher text and decrypts that with the nonce and the sha256(password+user_sauce).
///  Returns the initial secret.
///
/// @param text The encrypted text
/// @param nonce The nonce for the encrypted text
/// @param userSauce The password to decrypt the text
///
/// @returns The decrypted secret
Future<String> decryptSecret(
    Uint8List text, Uint8List nonce, String? password, String userSauce) async {
  String? salt = await sha512(userSauce);
  Uint8List k = converter.fromHex(
    await sha256(await passwordScrypt({'password': password, 'salt': salt})),
  )!;

  final box = SecretBox(k);
  var decryptedDate = box.decrypt(ByteList(text), nonce: nonce);
  return converter.decodeUTF8(decryptedDate);
}

/// Takes the data and the secret_key and encrypts the data.
/// Returns the nonce and the cipher text
///
/// @param data The data you want to encrypt
/// @param secret_key The secret key you want to use to encrypt the data
///
/// @returns The encrypted text and the nonce
Future<EncryptedData> encryptData(String data, Uint8List secretKey) async {
  final Uint8List nonce = await randomBytes(24);
  final box = SecretBox(secretKey);

  var encryptedData = box.encrypt(
    converter.encodeUTF8(data),
    nonce: nonce,
  );

  return EncryptedData(Uint8List.fromList(encryptedData.cipherText), nonce);
}

/// Takes the cipher text and decrypts that with the nonce and the secret_key.
/// Returns the initial data.
///
/// @param text The encrypted text
/// @param text nonce The nonce of the encrypted text
/// @param secret_key The secret key used in the past to encrypt the text
///
/// @returns The decrypted data
Future<String> decryptData(
    Uint8List text, Uint8List nonce, Uint8List secretKey) async {
  final box = SecretBox(secretKey);
  var decryptedDate = box.decrypt(ByteList(text), nonce: nonce);

  return converter.decodeUTF8(decryptedDate);
}

/// Scrypt wrapper for psono to create for a password and a salt the fix scrypt hash.
///
/// @param kwargs the password and salt one wants to hash
///
/// @returns The scrypt hash
Future<String> passwordScrypt(Map<String, String?> kwargs) {
  String computeHelper(Map<String, String?> kwargs) {
    final int N = 16384; // 2^14
    final int r = 8;
    final int p = 1;
    final int l = 64;

    final Scrypt derivator = new Scrypt();
    derivator.init(
      ScryptParameters(
        N,
        r,
        p,
        l,
        converter.encodeUTF8(kwargs['salt']!),
      ),
    );

    final String scryptHex = converter
        .toHex(derivator.process(converter.encodeUTF8(kwargs['password']!)))!;

    return scryptHex;
  }

  return compute(computeHelper, kwargs);
}

/// takes the sha512 of lowercase username as salt to generate scrypt password hash in hex called
/// the authkey, so basically:
///
/// hex(scrypt(password, hex(sha512(lower(username)))))
///
/// For compatibility reasons with other clients please use the following parameters if you create your own client:
///
/// var c = 16384 // 2^14;
/// var r = 8;
/// var p = 1;
/// var l = 64;
/// @param username Username of the user (in email format)
/// @param password Password of the user
///
/// @returns auth_key Scrypt hex value of the password with the sha512 of lowercase email as salt
Future<String> generateAuthkey(String username, String password) async {
  username = username.toLowerCase();

  final String? salt = await sha512(username);
  // scrypt + sha512 candidate https://github.com/PointyCastle/pointycastle
  // sha512 candidate https://github.com/dart-lang/crypto/pull/63

  return passwordScrypt({'password': password, 'salt': salt});
}

/// takes the sha512 of lowercase username as salt to generate scrypt password hash in hex called
/// the authkey, so basically:
///
/// hex(scrypt(password, hex(sha512(lower(username)))))
///
/// For compatibility reasons with other clients please use the following parameters if you create your own client:
///
/// var c = 16384 // 2^14;
/// var r = 8;
/// var p = 1;
/// var l = 64;
/// @param username Username of the user (in email format)
/// @param password Password of the user
///
/// @returns auth_key Scrypt hex value of the password with the sha512 of lowercase email as salt
Future<Uint8List> generateSecretKey() async {
  return randomBytes(32);
}

/// generates a n-long base58 checksum
Future<String> getChecksum(String str, int n) async {
  return converter.hexToBase58(await sha512(str)).substring(0, n);
}

/// Removes the checksums from a base58 encoded recovery code with checksums.
/// e.g. 'UaKSKNNixJY2ARqGDKXduo4c2N' becomes 'UaKSKNNixJYRqGDKXduo4c'
Future<String> recoveryCodeStripChecksums(
    String recoveryCodeWithChecksums) async {
  var recoveryCodeChunks =
      helper.splitStringInChunks(recoveryCodeWithChecksums, 13);

  for (var i = 0; i < recoveryCodeChunks.length; i++) {
    if (recoveryCodeChunks[i].length < 2) {
      throw new InvalidRecoveryCodeException(
          "Recovery code chunks with a size < 2 are impossible");
    }
    recoveryCodeChunks[i] =
        recoveryCodeChunks[i].substring(0, recoveryCodeChunks[i].length - 2);
  }
  return recoveryCodeChunks.join('');
}

/// Tests if a given recovery password chunk can be valid according to the checksum
///  e.g. UaKSKNNixJY2A would return true and UaKSKNNixJY2B would return false
Future<bool> recoveryPasswordChunkPassChecksum(String chunkWithChecksum) async {
  if (chunkWithChecksum.length < 2) {
    return false;
  }
  String password =
      chunkWithChecksum.substring(0, chunkWithChecksum.length - 2);
  String checksum = chunkWithChecksum.substring(chunkWithChecksum.length - 2);

  return await getChecksum(password, 2) == checksum;
}

/// Generates a uuid
///
/// @returns Returns a random uuid
Future<String> generateUUID() async {
  var uuid = Uuid();
  return uuid.v4(options: {'rng': UuidUtil.cryptoRNG});
}
