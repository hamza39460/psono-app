import 'package:psono/model/secret.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/redux/store.dart';

/// Reads a secret and decrypts it. Returns the decrypted object
Future<Secret> readSecret(String secretId, Uint8List secretKey) async {
  apiClient.ReadSecret secret = await apiClient.readSecret(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    secretId,
  );

  String dataJson = await cryptoLibrary.decryptData(
      secret.data!, secret.dataNonce!, secretKey);

  Map<String, dynamic>? data = jsonDecode(dataJson);

  Secret decodedSecret = new Secret(
    createDate: secret.createDate,
    writeDate: secret.writeDate,
    secretId: secretId,
    type: secret.type,
    data: data,
    callbackUser: secret.callbackUser,
    callbackUrl: secret.callbackUrl,
    callbackPass: secret.callbackPass,
    secretKey: secretKey,
  );

  return decodedSecret;
}

/// Encrypts the content and creates a new secret out of it.
Future<Secret> createSecret(
  dynamic content,
  String linkId,
  String? parentDatastoreId,
  String? parentShareId,
  String callbackUrl,
  String callbackUser,
  String callbackPass,
) async {
  Uint8List secretKey = await cryptoLibrary.generateSecretKey();

  String jsonContent = jsonEncode(content);

  EncryptedData encryptedData =
      await cryptoLibrary.encryptData(jsonContent, secretKey);

  apiClient.CreateSecret secret = await apiClient.createSecret(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    encryptedData.text,
    encryptedData.nonce,
    linkId,
    parentDatastoreId,
    parentShareId,
    callbackUrl,
    callbackUser,
    callbackPass,
  );

  return Secret(
    secretId: secret.secretId,
    data: content,
    secretKey: secretKey,
  );
}

/// Encrypts some content and updates a secret with it. returns the secret id
Future<Secret> writeSecret(
  String? secretId,
  Uint8List secretKey,
  dynamic content,
  String? callbackUrl,
  String? callbackUser,
  String? callbackPass,
) async {
  String jsonContent = jsonEncode(content);

  EncryptedData encryptedData =
      await cryptoLibrary.encryptData(jsonContent, secretKey);

  apiClient.WriteSecret secret = await apiClient.writeSecret(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    secretId,
    encryptedData.text,
    encryptedData.nonce,
    callbackUrl,
    callbackUser,
    callbackPass,
  );

  return Secret(
    secretId: secret.secretId,
    data: content,
    secretKey: secretKey,
  );
}
