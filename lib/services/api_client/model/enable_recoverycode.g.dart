// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enable_recoverycode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EnableRecoverycode _$EnableRecoverycodeFromJson(Map<String, dynamic> json) =>
    EnableRecoverycode(
      recoveryData: fromHex(json['recovery_data'] as String?),
      recoveryDataNonce: fromHex(json['recovery_data_nonce'] as String?),
      recoverySauce: json['recovery_sauce'] as String?,
      userSauce: json['user_sauce'] as String?,
      verifierPublicKey: fromHex(json['verifier_public_key'] as String?),
      verifierTimeValid: json['verifier_time_valid'] as int?,
    );

Map<String, dynamic> _$EnableRecoverycodeToJson(EnableRecoverycode instance) =>
    <String, dynamic>{
      'recovery_data': toHex(instance.recoveryData),
      'recovery_data_nonce': toHex(instance.recoveryDataNonce),
      'recovery_sauce': instance.recoverySauce,
      'user_sauce': instance.userSauce,
      'verifier_public_key': toHex(instance.verifierPublicKey),
      'verifier_time_valid': instance.verifierTimeValid,
    };
