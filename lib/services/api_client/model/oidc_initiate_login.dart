import 'package:json_annotation/json_annotation.dart';

part 'oidc_initiate_login.g.dart';

@JsonSerializable()
class OidcInitiateLogin {
  OidcInitiateLogin({
    this.oidcRedirectUrl,
  });

  @JsonKey(name: 'oidc_redirect_url')
  final String? oidcRedirectUrl;

  factory OidcInitiateLogin.fromJson(Map<String, dynamic> json) =>
      _$OidcInitiateLoginFromJson(json);

  Map<String, dynamic> toJson() => _$OidcInitiateLoginToJson(this);
}
