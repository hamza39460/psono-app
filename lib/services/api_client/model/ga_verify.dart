import 'package:json_annotation/json_annotation.dart';

part 'ga_verify.g.dart';

@JsonSerializable()
class GaVerify {
  GaVerify();

  factory GaVerify.fromJson(Map<String, dynamic> json) =>
      _$GaVerifyFromJson(json);

  Map<String, dynamic> toJson() => _$GaVerifyToJson(this);
}
