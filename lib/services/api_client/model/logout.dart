import 'package:json_annotation/json_annotation.dart';

part 'logout.g.dart';

@JsonSerializable()
class Logout {
  Logout({
    this.success,
  });

  final String? success;

  factory Logout.fromJson(Map<String, dynamic> json) => _$LogoutFromJson(json);

  Map<String, dynamic> toJson() => _$LogoutToJson(this);
}
