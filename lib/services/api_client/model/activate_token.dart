import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'activate_token.g.dart';

@JsonSerializable()
class User {
  User({
    this.id,
    this.email,
    this.secretKey,
    this.secretKeyNonce,
  });

  final String? id;
  final String? email;
  @JsonKey(name: 'secret_key', fromJson: fromHex, toJson: toHex)
  final Uint8List? secretKey;
  @JsonKey(name: 'secret_key_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? secretKeyNonce;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}

@JsonSerializable()
class ActivateToken {
  ActivateToken({
    this.user,
  });

  final User? user;

  factory ActivateToken.fromJson(Map<String, dynamic> json) =>
      _$ActivateTokenFromJson(json);

  Map<String, dynamic> toJson() => _$ActivateTokenToJson(this);
}
