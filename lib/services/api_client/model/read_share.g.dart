// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_share.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadShareRight _$ReadShareRightFromJson(Map<String, dynamic> json) =>
    ReadShareRight(
      read: json['read'] as bool?,
      write: json['write'] as bool?,
      grant: json['grant'] as bool?,
    );

Map<String, dynamic> _$ReadShareRightToJson(ReadShareRight instance) =>
    <String, dynamic>{
      'read': instance.read,
      'write': instance.write,
      'grant': instance.grant,
    };

ReadShare _$ReadShareFromJson(Map<String, dynamic> json) => ReadShare(
      id: json['id'] as String?,
      data: fromHex(json['data'] as String?),
      dataNonce: fromHex(json['data_nonce'] as String?),
      userId: json['userId'] as String?,
      rights: json['rights'] == null
          ? null
          : ReadShareRight.fromJson(json['rights'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ReadShareToJson(ReadShare instance) => <String, dynamic>{
      'id': instance.id,
      'data': toHex(instance.data),
      'data_nonce': toHex(instance.dataNonce),
      'userId': instance.userId,
      'rights': instance.rights,
    };
