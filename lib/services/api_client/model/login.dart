import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'login.g.dart';

@JsonSerializable()
class Login {
  Login({
    this.loginInfo,
    this.loginInfoNonce,
  });

  @JsonKey(name: 'login_info', fromJson: fromHex, toJson: toHex)
  final Uint8List? loginInfo;
  @JsonKey(name: 'login_info_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List? loginInfoNonce;

  factory Login.fromJson(Map<String, dynamic> json) => _$LoginFromJson(json);

  Map<String, dynamic> toJson() => _$LoginToJson(this);
}
