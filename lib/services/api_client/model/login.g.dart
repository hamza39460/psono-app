// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Login _$LoginFromJson(Map<String, dynamic> json) => Login(
      loginInfo: fromHex(json['login_info'] as String?),
      loginInfoNonce: fromHex(json['login_info_nonce'] as String?),
    );

Map<String, dynamic> _$LoginToJson(Login instance) => <String, dynamic>{
      'login_info': toHex(instance.loginInfo),
      'login_info_nonce': toHex(instance.loginInfoNonce),
    };
