// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Info _$InfoFromJson(Map<String, dynamic> json) => Info(
      json['info'] as String?,
      fromHex(json['signature'] as String?),
      fromHex(json['verify_key'] as String?),
    );

Map<String, dynamic> _$InfoToJson(Info instance) => <String, dynamic>{
      'info': instance.info,
      'signature': toHex(instance.signature),
      'verify_key': toHex(instance.verifyKey),
    };
