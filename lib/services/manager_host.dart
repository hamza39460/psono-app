import 'dart:convert';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:psono/model/check_host_result.dart';
import 'package:psono/model/check_known_host_result.dart';
import 'package:psono/model/known_host.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/crypto_library.dart';
import 'package:psono/services/storage.dart';

List<KnownHost> getDefaultHosts() {
  return [
    new KnownHost(
      url: 'https://www.psono.pw/server',
      verifyKey: converter.fromHex(
        'a16301bd25e3a445a83b279e7091ea91d085901933f310fdb1b137db9676de59',
      ),
    )
  ];
}

/// Reads the stored known hosts (extend by the default trusted servers).
Future<List<KnownHost>> getKnownHosts() async {
  String? knownHostsJson = await storage.read(key: 'knownHosts');

  if (knownHostsJson == null) {
    List<KnownHost> defaultHosts = getDefaultHosts();

    knownHostsJson = jsonEncode(defaultHosts);
    storage.write(
        key: 'knownHosts', value: knownHostsJson, iOptions: secureIOSOptions);
  }

  Iterable l = jsonDecode(knownHostsJson);
  List<KnownHost> knownHostList = l.map((i) => KnownHost.fromJson(i)).toList();

  return knownHostList;
}

/// Updates teh stored known hosts
Future updateKnownHosts(List<KnownHost> knownHosts) async {
  String knownHostsString = jsonEncode(knownHosts);
  await storage.write(
      key: 'knownHosts', value: knownHostsString, iOptions: secureIOSOptions);
}

/// Approves a host by updating or adding it in the known hosts
Future approveHost(String serverUrl, Uint8List? verifyKey) async {
  var knownHosts = await getKnownHosts();

  serverUrl = serverUrl.toLowerCase();

  for (var i = 0; i < knownHosts.length; i++) {
    if (knownHosts[i].url != serverUrl) {
      continue;
    }
    if (converter.toHex(knownHosts[i].verifyKey) !=
        converter.toHex(verifyKey)) {
      knownHosts[i].verifyKey = verifyKey;
    }

    await updateKnownHosts(knownHosts);
    return;
  }

  knownHosts.add(KnownHost(url: serverUrl, verifyKey: verifyKey));

  await updateKnownHosts(knownHosts);
}

/// Compares a given url and fingerprint against the existing known hosts.
Future<CheckKnownHostResult> checkKnownHost(
    String serverUrl, Uint8List? verifyKey) async {
  var knownHosts = await getKnownHosts();

  serverUrl = serverUrl.toLowerCase();

  for (var i = 0; i < knownHosts.length; i++) {
    if (knownHosts[i].url != serverUrl) {
      continue;
    }
    if (converter.toHex(knownHosts[i].verifyKey) !=
        converter.toHex(verifyKey)) {
      return CheckKnownHostResult(
          status: 'signature_changed', verifyKeyOld: knownHosts[i].verifyKey);
    }

    return CheckKnownHostResult(status: 'matched');
  }

  return CheckKnownHostResult(status: 'not_found');
}

/// Takes the server url and the server info object and compares the public key
/// against the known ones in the storage.
Future<CheckHostResult> checkHost(String serverUrl, apiClient.Info info) async {
  serverUrl = serverUrl.toLowerCase();

  var valid =
      await validateSignature(info.info!, info.signature!, info.verifyKey!);

  if (!valid) {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'invalid_signature',
        verifyKey: null,
        info: info);
  }

  var checkResult = await checkKnownHost(serverUrl, info.verifyKey);

  if (checkResult.status == 'matched') {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'matched',
        verifyKey: info.verifyKey,
        info: info);
  } else if (checkResult.status == 'signature_changed') {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'signature_changed',
        verifyKey: checkResult.verifyKeyOld,
        info: info);
  } else {
    return CheckHostResult(
        serverUrl: serverUrl,
        status: 'new_server',
        verifyKey: info.verifyKey,
        info: info);
  }
}

/// Loads a remote config. It takes an url of a remote web client and loads its config.
Future<String> loadRemoteConfig(String serverUrl) async {
  apiClient.Info packageInfo = await apiClient.info();

  final client = http.Client();

  http.Request request = http.Request(
    'GET',
    Uri.parse("${packageInfo.webClient!}/config.json"),
  );
  var streamedResponse = await client.send(request).timeout(
        const Duration(seconds: 5),
      );
  http.Response response = await http.Response.fromStream(
    streamedResponse,
  );

  return response.body;
}
