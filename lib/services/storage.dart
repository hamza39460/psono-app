import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = new FlutterSecureStorage();
final secureIOSOptions =
    IOSOptions(accessibility: KeychainAccessibility.unlocked_this_device);
