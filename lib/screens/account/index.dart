import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/screens/custom_drawer.dart';

import './change_email.dart';
import './change_password.dart';
import './delete_account.dart';
import './overview.dart';

class AccountScreen extends StatefulWidget {
  static String tag = 'settings-screen';

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = [];

    bool complianceDisableDeleteAccount =
        reduxStore.state.complianceDisableDeleteAccount;

    menuList.add(
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'OVERVIEW')),
          leading: const Icon(component.FontAwesome.book),
          trailing: const Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AccountOverviewScreen(),
              ),
            );
          },
        ),
      ),
    );

    menuList.add(
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'CHANGE_E_MAIL')),
          leading: const Icon(component.FontAwesome.envelope),
          trailing: const Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AccountChangeEmailScreen(),
              ),
            );
          },
        ),
      ),
    );

    menuList.add(
      Card(
        child: ListTile(
          title: Text(FlutterI18n.translate(context, 'CHANGE_PASSWORD')),
          leading: const Icon(component.FontAwesome.key),
          trailing: const Icon(Icons.keyboard_arrow_right),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AccountChangePasswordScreen(),
              ),
            );
          },
        ),
      ),
    );

    if (!complianceDisableDeleteAccount) {
      menuList.add(
        Card(
          child: ListTile(
            title: Text(FlutterI18n.translate(context, 'DELETE_ACCOUNT')),
            leading: const Icon(component.FontAwesome.trash_o),
            trailing: const Icon(Icons.keyboard_arrow_right),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AccountDeleteAccountScreen(),
                ),
              );
            },
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(context, 'ACCOUNT')),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: const Color(0xFFebeeef),
      body: Container(
          padding: const EdgeInsets.all(5.0),
          child: ListView(
            children: ListTile.divideTiles(
              context: context,
              tiles: menuList,
            ).toList(),
          )),
      drawer: CustomDrawer(),
    );
  }
}
