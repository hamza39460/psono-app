import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;

class SettingAutofillIOSScreen extends StatefulWidget {
  static String tag = 'settings-autofill-screen';

  @override
  _SettingAutofillIOSScreenState createState() =>
      _SettingAutofillIOSScreenState();
}

class _SettingAutofillIOSScreenState extends State<SettingAutofillIOSScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'AUTOFILL_SERVICE',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: Card(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: new ListView(
              children: ListTile.divideTiles(
                context: context,
                tiles: [
                  component.AlertInfo(
                    text: FlutterI18n.translate(
                        context, "FOLLOW_STEPS_TO_ENABLE_AUTOFILL_SERVICE_IOS"),
                  ),
                  ListTile(
                    title: Text(
                        FlutterI18n.translate(context, 'OPEN_SETTINGS_APP')),
                    leading: Image(
                      image: AssetImage("assets/images/iconSettings.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  ListTile(
                    title: Text(FlutterI18n.translate(
                        context, 'TAP_INTO_PASSWORDS_AND_ACCOUNTS')),
                    leading: Image(
                      image:
                          AssetImage("assets/images/iconPasswordsAccounts.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  ListTile(
                    title: Text(FlutterI18n.translate(
                        context, 'TAP_INTO_AUTOFILL_PASSWORDSS')),
                    leading: Image(
                      image: AssetImage("assets/images/iconAutofill.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  ListTile(
                    title: Text(FlutterI18n.translate(
                        context, 'TURN_ON_AUTOFILL_PASSWORDS')),
                    leading: Image(
                      image: AssetImage("assets/images/iconOn.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  ListTile(
                    title: Text(FlutterI18n.translate(context, 'SELECT_PSONO')),
                    leading: Image(
                      image: AssetImage("assets/images/iconPsonoApp.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  ListTile(
                    title: Text(
                        FlutterI18n.translate(context, 'DESELECT_KEYCHAIN')),
                    leading: Image(
                      image:
                          AssetImage("assets/images/iconPasswordsAccounts.png"),
                      height: 30.0,
                      width: 30.0,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ],
              ).toList(),
            ),
          ),
        ),
      ),
    );
  }
}
