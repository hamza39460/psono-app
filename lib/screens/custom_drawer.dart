import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:package_info/package_info.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/theme.dart';

class CustomDrawer extends StatefulWidget {
  static String tag = 'datastore-screen';
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  String _versionInfo = '';

  Future<void> loadVersionInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    String appName = packageInfo.appName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;

    setState(() {
      _versionInfo = appName + ": " + version + " (build " + buildNumber + ")";
    });
  }

  @override
  void initState() {
    super.initState();
    loadVersionInfo();
  }

  @override
  Widget build(BuildContext context) {
    var route = ModalRoute.of(context);
    String? routeName = '';
    if (route != null) {
      routeName = route.settings.name;
    } else {
      routeName = '';
    }

    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: const Color(0xFF151f2b),
      ),
      child: Drawer(
        child: Column(
          children: <Widget>[
            Container(
              color: const Color(0xFFFFFFFF),
              width: double.infinity,
              height: 98.0,
              child: DrawerHeader(
                margin: const EdgeInsets.all(0.0),
                padding: const EdgeInsets.all(10.0),
                child: Hero(
                  tag: 'hero',
                  child: SvgPicture.asset(
                    'assets/images/logo_green.svg',
                    semanticsLabel: 'Psono Logo',
                    height: 70.0,
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  new Radius.circular(5.0),
                ),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "NAVIGATION"),
                  style: const TextStyle(
                    color: Color(0xFFb1b6c1),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: routeName == '/datastore/'
                    ? primarySwatch.shade500
                    : const Color(0xFF151f2b),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "HOME"),
                  style: TextStyle(
                    color: routeName == '/datastore/'
                        ? const Color(0xFFFFFFFF)
                        : const Color(0xFFb1b6c1),
                  ),
                ),
                leading: Icon(
                  component.FontAwesome.home,
                  color: routeName == '/datastore/'
                      ? const Color(0xFFFFFFFF)
                      : const Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/datastore/');
                },
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: routeName == '/settings/'
                    ? primarySwatch.shade500
                    : const Color(0xFF151f2b),
                borderRadius: BorderRadius.all(
                  new Radius.circular(5.0),
                ),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "SETTINGS"),
                  style: TextStyle(
                    color: routeName == '/settings/'
                        ? const Color(0xFFFFFFFF)
                        : const Color(0xFFb1b6c1),
                  ),
                ),
                leading: Icon(
                  component.FontAwesome.cogs,
                  color: routeName == '/settings/'
                      ? const Color(0xFFFFFFFF)
                      : const Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/settings/');
                },
              ),
            ),
            Container(
              decoration: BoxDecoration(
                color: routeName == '/account/'
                    ? primarySwatch.shade500
                    : const Color(0xFF151f2b),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
              ),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "ACCOUNT"),
                  style: TextStyle(
                    color: routeName == '/account/'
                        ? const Color(0xFFFFFFFF)
                        : const Color(0xFFb1b6c1),
                  ),
                ),
                leading: Icon(
                  component.FontAwesome.sliders,
                  color: routeName == '/account/'
                      ? const Color(0xFFFFFFFF)
                      : const Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  Navigator.pushReplacementNamed(context, '/account/');
                },
              ),
            ),
            Container(
              color: const Color(0xFF151f2b),
              child: ListTile(
                title: Text(
                  FlutterI18n.translate(context, "LOGOUT"),
                  style: const TextStyle(
                    color: Color(0xFFb1b6c1),
                  ),
                ),
                leading: const Icon(
                  component.FontAwesome.sign_out,
                  color: Color(0xFFb1b6c1),
                ),
                onTap: () async {
                  managerDatastoreUser.logout();
                  Navigator.pushReplacementNamed(context, '/signin/');
                },
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  child: ListTile(
                    title: Text(
                      this._versionInfo,
                      style: const TextStyle(
                        color: Color(0xFF444851),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
