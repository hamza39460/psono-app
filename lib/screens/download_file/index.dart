import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;

class DownloadFileScreen extends StatefulWidget {
  static String tag = 'download-file-screen';
  final datastoreModel.Folder? parent;
  final datastoreModel.Datastore? datastore;
  final datastoreModel.Folder? share;
  final datastoreModel.Item item;
  final List<String?>? path;
  final List<String?>? relativePath;

  const DownloadFileScreen({
    this.parent,
    this.datastore,
    this.share,
    required this.item,
    this.path,
    this.relativePath,
  });

  @override
  _DownloadFileScreenState createState() => _DownloadFileScreenState();
}

class _DownloadFileScreenState extends State<DownloadFileScreen> {
  datastoreModel.Item? item;

  final logo = Hero(
    tag: 'hero',
    child: SvgPicture.asset(
      'assets/images/logo.svg',
      semanticsLabel: 'Psono Logo',
      height: 70.0,
    ),
  );

  _DownloadFileScreenState({this.item});

  @override
  Widget build(BuildContext context) {
    return component.ScaffoldDark(
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: const EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            const SizedBox(height: 56.0),
            component.AlertInfo(
              text: FlutterI18n.translate(
                context,
                "FILES_NOT_SUPPORTED",
              ),
            ),
            const SizedBox(height: 8.0),
            Row(
              children: <Widget>[
                component.BtnPrimary(
                  onPressed: () async {
                    Navigator.pop(context);
                  },
                  text: FlutterI18n.translate(context, "BACK"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
