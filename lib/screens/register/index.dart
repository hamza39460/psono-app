import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/services/storage.dart';
import 'package:url_launcher/url_launcher.dart';

class RegisterScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _usernameController = TextEditingController(
    text: '',
  );
  final _emailController = TextEditingController(
    text: '',
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  String _screen = 'default';
  bool _obscurePassword = true;
  late apiClient.Info info;

  String _domainSuffix = '@${helper.getDomain(reduxStore.state.serverUrl)!}';

  @override
  void dispose() {
    component.Loader.hide();
    _usernameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordRepeatController.dispose();
    _serverUrlController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: SvgPicture.asset(
        'assets/images/logo.svg',
        semanticsLabel: 'Psono Logo',
        height: 70.0,
      ),
    );

    final username = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "USERNAME"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 13.0, 20.0, 10.0),
          child: Text(
            _domainSuffix,
            style: const TextStyle(color: Color(0xFFb1b6c1), fontSize: 16.0),
          ),
        ),
      ),
    );

    final email = TextFormField(
      controller: _emailController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "EMAIL"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "PASSWORD"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: const Color(0xFF2dbb93),
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    final passwordRepeat = TextFormField(
      controller: _passwordRepeatController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "PASSWORD_REPEAT"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: const Color(0xFF2dbb93),
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    void onChange() {
      if (_usernameController.text.contains('@')) {
        _domainSuffix = '';
      } else {
        _domainSuffix = '@' + helper.getDomain(_serverUrlController.text)!;
      }

      setState(() {
        _domainSuffix = _domainSuffix;
      });
    }

    _usernameController.addListener(onChange);
    _serverUrlController.addListener(onChange);

    void _showErrorDiaglog(String title, String? content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(FlutterI18n.translate(context, title)),
            content: Text(FlutterI18n.translate(context, content!)),
            actions: <Widget>[
              TextButton(
                child: Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    void initiateRegistration(
        String email, String username, String password) async {
      component.Loader.show(context);
      try {
        await managerDatastoreUser.register(
          email,
          username,
          password,
          helper.getDomain(_serverUrlController.text),
        );
      } on apiClient.BadRequestException catch (e) {
        _showErrorDiaglog('ERROR', e.getFirst());
        return;
      } finally {
        component.Loader.hide();
      }
      setState(() {
        _screen = 'success';
      });
    }

    final registerButton = component.BtnPrimary(
      onPressed: () async {
        try {
          await storage.write(
            key: 'serverUrl',
            value: _serverUrlController.text,
            iOptions: secureIOSOptions,
          );
          await storage.write(
            key: 'username',
            value: _usernameController.text + _domainSuffix,
            iOptions: secureIOSOptions,
          );

          reduxStore.dispatch(
            InitiateLoginAction(
              _serverUrlController.text,
              _usernameController.text + _domainSuffix,
            ),
          );
          info = await apiClient.info();
        } on apiClient.ServiceUnavailableException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        String? testError = helper.isValidPassword(
          _passwordController.text,
          _passwordRepeatController.text,
          info.complianceMinMasterPasswordLength,
          info.complianceMinMasterPasswordComplexity,
        );
        if (testError != null) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        bool isValid = helper.isValidEmail(_emailController.text);
        if (!isValid) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, 'INVALID_EMAIL_FORMAT'),
          );
          return;
        }

        String username = _usernameController.text + _domainSuffix;

        testError = helper.isValidUsername(username);
        if (testError != null) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        initiateRegistration(
            _emailController.text, username, _passwordController.text);
      },
      text: FlutterI18n.translate(context, "REGISTER"),
    );

    final server = TextFormField(
      keyboardType: TextInputType.url,
      controller: _serverUrlController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "SERVER"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    if (_screen == 'success') {
      return component.ScaffoldDark(
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              const SizedBox(height: 24.0),
              const Icon(
                component.FontAwesome.thumbs_o_up,
                color: Color(0xFFFFFFFF),
                size: 48.0,
              ),
              const SizedBox(height: 24.0),
              component.AlertInfo(
                text: FlutterI18n.translate(context, "SUCCESSFUL_CHECK_EMAIL"),
              ),
              const SizedBox(height: 24.0),
              component.BtnPrimary(
                text: FlutterI18n.translate(context, "BACK_TO_HOME"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      );
    } else {
      return component.ScaffoldDark(
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              const SizedBox(height: 24.0),
              username,
              const SizedBox(height: 8.0),
              email,
              const SizedBox(height: 8.0),
              password,
              const SizedBox(height: 8.0),
              passwordRepeat,
              const SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: registerButton,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 5.0),
                      child: TextButton(
                        style: TextButton.styleFrom(
                          foregroundColor: const Color(0xFFb1b6c1),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(FlutterI18n.translate(context, "ABORT")),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 24.0),
              server,
              const SizedBox(height: 16.0),
              Center(
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                        style: const TextStyle(color: Color(0xFF666666)),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () async {
                            final url = Uri.parse(
                                'https://www.psono.pw/privacy-policy.html');
                            if (await canLaunchUrl(url)) {
                              await launchUrl(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
