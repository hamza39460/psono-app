//
//  JsonObject.swift
//  AutoFillExtension
//

import Foundation

protocol JsonObject {
    init()
    init?(jsonDictionary: [String: Any])
    func toJson() -> String?
}
