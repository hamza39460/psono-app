//
//  ManagerBase.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class ManagerBaseService {
    
    
    /**
     Encrypts some data with  the user's secret key
     
     - Parameter data: the data to encrypt
     
     - Returns: The encrypted text and nonce
     */
    public func encryptSecretKey(data: String) -> (Bytes, Bytes)  {
        
        let secretKey: Bytes? = converter.fromHex(string: storage.read(key: "secretKey"))
        
        return cryptoLibrary.encryptData(data: data, secretKey: secretKey!)
    }
    
    
    /**
     Decrypts an encrypted text with a nonce and the user's secret key
     
     - Parameter text: the encrypted text as bytes
     - Parameter nonce: the nonce of the encrypted text
     
     - Returns: Returns the decrypted data
     */
    public func decryptSecretKey(text: Bytes, nonce: Bytes) -> String?  {
        
        let secretKey: Bytes? = converter.fromHex(string: storage.read(key: "secretKey"))
        
        guard secretKey != nil else {
            return nil
        }
        
        return cryptoLibrary.decryptData(text: text, nonce: nonce, secretKey: secretKey!)
    }
}

let managerBase = ManagerBaseService()
