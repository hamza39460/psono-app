//
//  ReadDatastoreList.swift
//  AutoFillExtension
//

import Foundation


class ReadDatastoreList: JsonObject {
    public var datastores: [ReadDatastoreListEntry]?
    
    required init() {
        
    }
    
    public required init?(jsonDictionary: [String: Any]){
        
        if jsonDictionary["datastores"] != nil {
            if let datastoresJson = jsonDictionary["datastores"] as? [[String: Any]] {
                self.datastores = []
                for datastoreJson in datastoresJson {
                    let datastore = ReadDatastoreListEntry(jsonDictionary: datastoreJson)
                    if datastore != nil {
                        self.datastores?.append(datastore!)
                    }
                }
            }
            
        }
        
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.datastores != nil {
            var datastores: [[String: Any]] = []
            for datastore in self.datastores! {
                datastores.append(datastore.toJsonDict())
            }
            jsonDict["datastores"] = datastores
        }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
        
    }
}


class ReadDatastoreListEntry: JsonObject {
    public var id: String?
    public var type: String?
    public var description: String?
    public var isDefault: Bool?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.id = jsonDictionary["id"] as? String
        self.type = jsonDictionary["type"] as? String
        self.description = jsonDictionary["description"] as? String
        self.isDefault = jsonDictionary["is_default"] as? Bool
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.id != nil { jsonDict["id"] = self.id }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.description != nil { jsonDict["description"] = self.description }
        if self.isDefault != nil { jsonDict["is_default"] = self.isDefault }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
