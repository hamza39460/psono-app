//
//  ReadDatastore.swift
//  AutoFillExtension
//

import Foundation
import Sodium


class ReadDatastore: JsonObject {
    public var data: Bytes?
    public var dataNonce: Bytes?
    public var type: String?
    public var description: String?
    public var secretKey: Bytes?
    public var secretKeyNonce: Bytes?
    public var isDefault: Bool?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.data = converter.fromHex(string: jsonDictionary["data"] as? String)
        self.dataNonce = converter.fromHex(string: jsonDictionary["data_nonce"] as? String)
        self.type = jsonDictionary["type"] as? String
        self.description = jsonDictionary["description"] as? String
        self.secretKey = converter.fromHex(string: jsonDictionary["secret_key"] as? String)
        self.secretKeyNonce = converter.fromHex(string: jsonDictionary["secret_key_nonce"] as? String)
        self.isDefault = jsonDictionary["is_default"] as? Bool
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.data != nil { jsonDict["data"] = converter.toHex(bytes: self.data) }
        if self.dataNonce != nil { jsonDict["data_nonce"] = converter.toHex(bytes: self.dataNonce) }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.description != nil { jsonDict["description"] = self.description }
        if self.secretKey != nil { jsonDict["secret_key"] = converter.toHex(bytes: self.secretKey) }
        if self.secretKeyNonce != nil { jsonDict["secret_key_nonce"] = converter.toHex(bytes: self.secretKeyNonce) }
        if self.isDefault != nil { jsonDict["is_default"] = self.isDefault }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
