//
//  Share.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class Share: JsonObject {
    public var shareId: String?
    public var shareSecretKey: Bytes?
    public var folder: Folder?
    public var item: Item?
    public var rights: ShareRight?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.shareId = jsonDictionary["share_id"] as? String
        self.shareSecretKey = converter.fromHex(string: jsonDictionary["share_secret_key"] as? String)
        
        if jsonDictionary["folder"] != nil {
            self.folder = Folder.init(jsonDictionary: jsonDictionary["folder"] as! [String: Any])
        }
        if jsonDictionary["item"] != nil {
            self.item = Item.init(jsonDictionary: jsonDictionary["item"] as! [String: Any])
        }
        if jsonDictionary["rights"] != nil {
            self.rights = ShareRight.init(jsonDictionary: jsonDictionary["rights"] as! [String: Any])
        }
        
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.shareId != nil { jsonDict["share_id"] = self.shareId }
        if self.shareSecretKey != nil { jsonDict["share_secret_key"] = converter.toHex(bytes: self.shareSecretKey) }
        if self.folder != nil { jsonDict["folder"] = self.folder!.toJsonDict() }
        if self.item != nil { jsonDict["item"] = self.item!.toJsonDict() }
        if self.rights != nil { jsonDict["rights"] = self.rights!.toJsonDict() }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
