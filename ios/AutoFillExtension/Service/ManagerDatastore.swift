//
//  ManagerDatastore.swift
//  AutoFillExtension
//

import Foundation


class ManagerDatastoreService {
    
    /**
     Returns an overview of all the datastores
     
     - Parameter completionHandler: The completion handler that will receive the list of datastores
     */
    public func getDatastoreOverview(completionHandler: @escaping (ReadDatastoreList?, Error?) -> Void) {
        
        return apiClient.readDatastoreList(
            token: storage.read(key: "token"),
            sessionSecretKey: storage.read(key: "sessionSecretKey"),
            completionHandler: completionHandler
        )
    }
    
    /**
     Returns the datastore with the given datastore id
     
     - Parameter datastoreId; The id of the datastore
     */
    public func getDatastoreWithId(datastoreId: String, completionHandler: @escaping (Datastore?, Error?) -> Void) {
        
        apiClient.readDatastore(token: storage.read(key: "token"), sessionSecretKey: storage.read(key: "sessionSecretKey"), datastoreId: datastoreId, completionHandler: {
            (datastore: ReadDatastore?, error: Error?) -> Void in
            guard datastore != nil else {
                completionHandler(nil, nil)
                return
            }
            
            let datastoreSecretKeyHex: String? = managerBase.decryptSecretKey(text: datastore!.secretKey!, nonce: datastore!.secretKeyNonce!)
            
            guard datastore!.data != nil && datastore?.dataNonce != nil && datastore!.data!.count > 0 else {
                // TODO return emote datastore
                completionHandler(nil, nil)
                return
            }
            
            let dataJson: String? = cryptoLibrary.decryptData(
                text: datastore!.data!,
                nonce: datastore!.dataNonce!,
                secretKey: converter.fromHex(string: datastoreSecretKeyHex)!
            )
            
            
            guard let bodyJson = try? JSONSerialization.jsonObject(with: dataJson!.data(using: .utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                completionHandler(nil, nil)
                return
            }
            
            guard let dataDictionary: [String: Any] = bodyJson as? [String: Any] else {
                completionHandler(nil, nil)
                return
            }
            
            
            let jsonDictionary: [String: Any] = ["datastore_id" : datastoreId, "data" : dataDictionary]
            
            let decodedDatastore: Datastore? = Datastore.init(jsonDictionary: jsonDictionary)
            
            completionHandler(decodedDatastore, nil)
        })
    }
    
    
    /**
     Returns the datastore with the given type.
     
     - Parameter id; The id of the datastore
     */
    public func getDatastoreId(type: String, completionHandler: @escaping (String?, Error?) -> Void) {
        
        managerDatastore.getDatastoreOverview(completionHandler: {
            (data: ReadDatastoreList?, error: Error?) -> Void in
            guard data != nil else {
                completionHandler(nil, nil)
                return
            }
            guard data!.datastores != nil else {
                completionHandler(nil, nil)
                return
            }
            let datastore = data!.datastores?.first(where: {$0.isDefault != nil && $0.isDefault! && $0.type != nil && $0.type! == type})
            
            guard datastore != nil else {
                completionHandler(nil, nil)
                return
            }
            
            completionHandler(datastore!.id!, nil)
            
        })
    }
    
    
    
    /**
     Returns the datastore with the given typen or id
     
     - Parameter id; The id of the datastore
     - Parameter completionHandler: The callback function to call that will contain the datastore or an error
     */
    public func getDatastore(type: String, id: String?, completionHandler: @escaping (Datastore?, Error?) -> Void) {
        
        if id == nil {
            self.getDatastoreId(type: type, completionHandler: {
                (id: String?, error: Error?) -> Void in
                
                guard id != nil else {
                    // Usually we would create here now a datastore, yet the swift autofill component probably doesn't need it
                    completionHandler(nil, nil)
                    return
                }
                
                self.getDatastoreWithId(datastoreId: id!, completionHandler: completionHandler)
            })
        }
        
    }
    
    
}

let managerDatastore = ManagerDatastoreService()
